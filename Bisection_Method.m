function [Bisect_Implied_Call_Vol,Bisect_Implied_Put_Vol,Bisect_Call_Count,Bisect_Put_Count]=Bisection_Method(S,K,r,T,P)
% This is a function that approximates via bisection method the implied
% volatility of a European style option:
%
% Output is in decimal form
%
% S = Underlying price per share in home currency
% K = Strike price per share in home currency
% r = Riskfree interest rate continuously compounded input as decimal
% T = Tenor - time to expiration input as decimal years
% P = Option price in home currency

%Array length
steps=100000;

%initiate arrays of B-S values
V=zeros(steps,1);
d1=zeros(steps,1);
d2=zeros(steps,1);
CallPrice=zeros(steps,1);
PutPrice=zeros(steps,1);
Vega=zeros(steps,1);
Price=P.*ones(steps,1);
Implied_Volatility=zeros(steps,1);

%Price Euro Put and Euro Call Price wrt Volatility
for j = 1 : steps
    V(j)=j./10000;
    d1(j)=(log(S./K)+(r+((V(j).^2)./2)).*T)./(V(j).*(T.^0.5));
    d2(j)=(log(S./K)+(r-((V(j).^2)./2)).*T)/(V(j).*(T.^0.5));
    CallPrice(j)=S.*cdf('norm',d1(j),0,1)-K.*exp(-r.*T).*cdf('norm',d2(j),0,1); % generates theoretical European Call price
    PutPrice(j)=K.*exp(-r.*T).*cdf('norm',-d2(j),0,1)-S.*cdf('norm',-d1(j),0,1); % generates theoretical European Put price
    Implied_Volatility(j)=j./10000;
end

%Plot price graphs
figure
plot(V, PutPrice, V, CallPrice, V, Price);
legend('Euro Put Price wrt Volatility', 'Euro Call Price wrt Volatility', 'Option price in Home Currency')
grid on;




%% Begin Bisection method algorithm
% Specify initial end points and midpoint indices and prices
% Lower Bound
LB_Call_Index=1;
LB_CallPrice = CallPrice(LB_Call_Index); %Lower bound Call Price
LB_Put_Index=1;
LB_PutPrice = PutPrice(LB_Put_Index); %Lower bound Put Price

% Upper Bound
UB_Call_Index=steps;
UB_CallPrice = CallPrice(UB_Call_Index); %Upper bound Call Price
UB_Put_Index=steps;
UB_PutPrice = PutPrice(UB_Put_Index); %Upper bound Put Price

% Mid Point
MP_Call_Index=steps./2;%initial Midpoint Call index value
MP_CallPrice=CallPrice(MP_Call_Index); %initial Midpoint Call Price
MP_Put_Index=steps./2;%initial Midpoint Put index value
MP_PutPrice=PutPrice(MP_Put_Index); %initial Midpoint Put Price

%Start bisection counter
Bisect_Call_Count = 0;
Bisect_Put_Count = 0;

%% Check which half contains solution and reset bounds
% If the midpoint option price is greater than the user specified price
% the implied volatility is in the lower bound half of the end points

%Call
while (MP_CallPrice-P).^2 > 0.00001
    if MP_CallPrice>P
        UB_Call_Index=MP_Call_Index;
        UB_CallPrice=CallPrice(UB_Call_Index);
        % LB stays as is
        MP_Call_Index=MP_Call_Index-round((UB_Call_Index-LB_Call_Index)./2,0); % Move midpoint
        MP_CallPrice=CallPrice(MP_Call_Index);
        Bisect_Call_Count = Bisect_Call_Count + 1;
    elseif MP_CallPrice<P
        % UB Stays as is
        LB_Call_Index=MP_Call_Index;
        LB_CallPrice=CallPrice(LB_Call_Index);
        MP_Call_Index=MP_Call_Index+round((UB_Call_Index-LB_Call_Index)./2,0); % Move midpoint
        MP_CallPrice=CallPrice(MP_Call_Index);
        Bisect_Call_Count = Bisect_Call_Count + 1;
    else
        Bisect_Implied_Call_Vol=Implied_Volatility(MP_Call_Index);
        Bisect_Call_Count = Bisect_Call_Count + 1;
    end
end

%Put
while (MP_PutPrice-P).^2 > 0.00001
    if MP_PutPrice>P % Intercept in lower half
        UB_Put_Index=MP_Put_Index;
        %UB_PutPrice=PutPrice(UB_Put_Index);
        % LB stays as is
        MP_Put_Index=MP_Put_Index-round((UB_Put_Index-LB_Put_Index)./2,0); % Move midpoint
        MP_PutPrice=PutPrice(MP_Put_Index);
        Bisect_Put_Count = Bisect_Put_Count + 1;
    elseif MP_PutPrice<P % Intercept in upper half
        % UB Stays as is
        LB_Put_Index=MP_Put_Index;
        %LB_PutPrice=PutPrice(LB_Put_Index);
        MP_Put_Index=MP_Put_Index+round((UB_Put_Index-LB_Put_Index)./2,0); % Move midpoint
        MP_PutPrice=PutPrice(MP_Put_Index);
        Bisect_Put_Count = Bisect_Put_Count + 1;
    else
        Bisect_Implied_Put_Vol=Implied_Volatility(MP_Put_Index);
    end
end
% Assign implied volatilities to output variables
Bisect_Implied_Call_Vol=Implied_Volatility(MP_Call_Index)
Bisect_Implied_Put_Vol=Implied_Volatility(MP_Put_Index)
end