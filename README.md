# README #

This is a function that approximates via bisection method the implied 
volatility of a European style option:

Output is in decimal form

S = Underlying price per share in home currency
K = Strike price per share in home currency
r = Riskfree interest rate continuously compounded input as decimal
T = Tenor - time to expiration input as decimal years
P = Option price in home currency