function [Newton_Implied_Call_Vol,Newton_Implied_Put_Vol,Newton_Call_Count,Newton_Put_Count]=Newton_Method(S,K,r,T,P)
% This is a function that approximates via Newton's method the implied
% volatility of a European style option:
%
% Output is in decimal form
%
% S = Underlying price per share in home currency
% K = Strike price per share in home currency
% r = Riskfree interest rate continuously compounded input as decimal
% T = Tenor - time to expiration input as decimal years
% P = Option price in home currency

%Array length
steps=100000;

%initiate arrays of B-S values
V=zeros(steps,1);
d1=zeros(steps,1);
d2=zeros(steps,1);
CallPrice=zeros(steps,1);
PutPrice=zeros(steps,1);
Vega=zeros(steps,1);
Price=P.*ones(steps,1);
Implied_Volatility=zeros(steps,1);

%Price Euro Put and Euro Call Price wrt Volatility
for j = 1 : steps
    V(j)=j./10000;
    d1(j)=(log(S./K)+(r+((V(j).^2)./2)).*T)./(V(j).*(T.^0.5));
    d2(j)=(log(S./K)+(r-((V(j).^2)./2)).*T)./(V(j).*(T.^0.5));
    CallPrice(j)=S.*cdf('norm',d1(j),0,1)-K.*exp(-r.*T).*cdf('norm',d2(j),0,1); % generates theoretical European Call price
    PutPrice(j)=K.*exp(-r.*T).*cdf('norm',-d2(j),0,1)-S.*cdf('norm',-d1(j),0,1); % generates theoretical European Put price
    Vega(j)=S.*pdf('norm',d1(j),0,1).*sqrt(T); % generates theoretical Vega
    Implied_Volatility(j)=j./10000;
end

%Plot price graphs
figure
plot(V, PutPrice, V, CallPrice, V, Price);
legend('Euro Put Price wrt Volatility', 'Euro Call Price wrt Volatility', 'Option price in Home Currency')
grid on;

%% Begin Newton's method algorithm
% Specify initial guess
POT_Call_Index = 2000; % Point of tangency (POT) Call Price index
POT_Put_Index = 2000; % Point of tangency (POT)Put Price index
POT_CallPrice = CallPrice(POT_Call_Index); % Initial Call Price
POT_PutPrice = PutPrice(POT_Call_Index); % Initial Call Price

%Start adjustment function
Delta_POT_Call=(P-POT_CallPrice)./Vega(POT_Call_Index);
Delta_POT_Put=(P-POT_PutPrice)./Vega(POT_Put_Index);

%Start  counter
Newton_Call_Count = 0;
Newton_Put_Count = 0;

%% Start Adjusting the volatility per adjustment function
%Call
while P-POT_CallPrice > 0.005
    POT_Call_Index = POT_Call_Index + (round(Delta_POT_Call.*10000,0)); % Moves index out to a new, adjusted value
    POT_CallPrice = CallPrice(POT_Call_Index); % update call price
    Delta_POT_Call=(P-POT_CallPrice)./Vega(POT_Call_Index); % new adjustment
    Newton_Call_Count = Newton_Call_Count + 1;
end
%Put
while P-POT_PutPrice > 0.005
    POT_Put_Index = POT_Put_Index + (round(Delta_POT_Put.*10000,0)); % Moves index out to a new, adjusted value
    POT_PutPrice = PutPrice(POT_Put_Index); % update put price
    Delta_POT_Put=(P-POT_PutPrice)./Vega(POT_Put_Index); % new adjustment
    Newton_Put_Count = Newton_Put_Count + 1;
end
% Assign implied volatilities to output variables
Newton_Implied_Call_Vol=Implied_Volatility(POT_Call_Index)
Newton_Implied_Put_Vol=Implied_Volatility(POT_Put_Index)
end